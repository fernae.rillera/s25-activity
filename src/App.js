import React from 'react';
import NavBar from './components/NavBar';
import './App.css';
// import Home from './pages/Home';
// import CoursesPage from './pages/CoursesPage';
// import Register from './pages/Register';
import Login from './pages/Login';

function App() {
  return (
  <React.Fragment>
   <NavBar />
   <Login />
  </React.Fragment>
  );
}

export default App;
