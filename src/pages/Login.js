import React, { useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';


export default function Register(){
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	// const input = document.querySelector('#email');


	function registerUser(e){
		e.preventDefault();

		

		

		if (email == 'juantamad@gmail.com'){
			console.log('incorrect username and password')
		}else{
			console.log('You have successfully logged in');
			setEmail('');
			setPassword('');

		}


	}
		useEffect(() => {
			if(email !== '' && password !== ''){
				setIsActive(true);
			
		}else{
				setIsActive(false);
		}
		},[email, password]);


	return(
		<Form onSubmit={e => registerUser(e)} className="col-lg-4 offset-lg-4 my-5">
			<Form.Group>
				<Form.Label>Email Address</Form.Label>
				<Form.Control 
					type="email"
					placeholder="Enter email"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control 
					type="password"
					placeholder="Password"
					value={password}
					onChange={e => setPassword(e.target.value)}
					required
				/>
			</Form.Group>


			{isActive 
				?	
			<Button className="bg-primary" type="submit" id="
			submitButton">
			submit
			</Button>
			:
			<Button className="bg-primary" type="submit" id="
			submitButton" disabled>
			submit
			</Button>
			
			}

		</Form>

		)
};


